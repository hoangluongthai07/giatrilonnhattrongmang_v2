﻿using System;

class Program
{
    static void Main()
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        int[] numbers = { 5, 10, 3, 8, 15, 7 };

        int largestNumber = FindLargestNumber(numbers);

        Console.WriteLine($"Số lớn nhất trong mảng là: {largestNumber}");
    }

    static int FindLargestNumber(int[] array)
    {
        if (array == null || array.Length == 0)
        {
            throw new ArgumentException("Mảng không hợp lệ.");
        }

        int largestNumber = array[0];

        for (int i = 1; i < array.Length; i++)
        {
            if (array[i] > largestNumber)
            {
                largestNumber = array[i];
            }
        }

        return largestNumber;
    }
}
